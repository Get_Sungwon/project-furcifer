﻿using UnityEngine;
using System.Collections;

public class Runner : MonoBehaviour
{
    public GameManager game;
    public RoadManager road;
    public GameObject road2;
    public float jumpPow = 2.3f;
    public int speed;
    public int life = 3;
    bool isstop = false;
    public bool nojump = false;


    Rigidbody runner;
    bool isJump;
    public Vector3 position;

    public void stop()
    {
        isstop = true;
    }

    public void restart()
    {
        isstop = false;
    }

    // Use this for initialization
    void Start()
    {
        runner = GetComponent<Rigidbody>();    
    }

    // Update is called once per frame
    void Update()
    {
        speed = game.speed;
        if (road.isTroad != 1 && !isstop)
            runner.transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.World);
        else if(!isstop)
        {
            if (road.Road2.transform.position.z + 195.5 > transform.position.z)
                runner.transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.World);

            if (road.Road2.transform.position.z + 195.5 <= transform.position.z && game.select == 0)
            {
                game.rotate(-1);
                road.isTroad = 0;
            }
            else if (road.Road2.transform.position.z + 195.5 <= transform.position.z && game.select == 1)
            {
                game.rotate(1);
                road.isTroad = 0;
            }
        }

        position = runner.position;
        if (Input.GetButtonDown("Jump") && runner.position.y <= 1.5)
            if (nojump)
            {
                if (game.select == 0)
                    game.select = 1;
                else
                    game.select = 0;
            }
            else
                isJump = true;
    }

    void FixedUpdate()
    {
        Jumping();
    }


    void Jumping()
    {
        if (!isJump)
            return;

        runner.AddForce(Vector3.up * jumpPow * 3, ForceMode.Impulse);
        isJump = false;
    }


}

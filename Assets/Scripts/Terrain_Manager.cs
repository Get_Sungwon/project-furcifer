﻿using UnityEngine;
using System.Collections;

public class Terrain_Manager : MonoBehaviour {

    public GameManager game;
    public Runner runner;
    public GameObject t1, t2, t3, t4;
    public GameObject[] terr;
    public float speed = 70f;
    public Vector3 position;
    bool isstop = false;

    public void stop()
    {
        isstop = true;
    }

    public void restart()
    {
        isstop = false;
    }

    // Use this for initialization
    void Start () {
        position = runner.position;
        Vector3 pos = new Vector3(position.x - 250, 0, position.z + 750);
	    t3 = Instantiate(terr[0], pos, Quaternion.identity) as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        position = runner.position;
        if (!isstop)
            MoveTerr();
    }

    public void rotate(int direction)
    {
        if (t1 != null)
            t1.transform.RotateAround(runner.position, new Vector3(0, 1, 0), 90 * direction);
        if (t2 != null)
            t2.transform.RotateAround(runner.position, new Vector3(0, 1, 0), 90 * direction);
        if (t3 != null)
            t3.transform.RotateAround(runner.position, new Vector3(0, 1, 0), 90 * direction);
        if (t4 != null)
            t4.transform.RotateAround(runner.position, new Vector3(0, 1, 0), 90 * direction);
    }

    void MoveTerr()
    {
        if (t1.transform.position.z - position.z <= -750)
        {
            Vector3 pos1 = new Vector3(position.x - 250, 0, position.z + 730);
            t4 = Instantiate(terr[0], pos1, Quaternion.identity) as GameObject;
            t4.transform.parent = transform;
            DestroyGround();
        }
    }

    void DestroyGround()
    {
        Destroy(t1);
        t1 = t2;
        t2 = t3;
        t3 = t4;
    }
}

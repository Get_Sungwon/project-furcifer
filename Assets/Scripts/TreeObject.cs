﻿using UnityEngine;
using System.Collections;

public class TreeObject : MonoBehaviour {

    public GameManager game;
    public Runner runner;
    public GameObject tree1;
    public GameObject tree2;
    public GameObject tree3;
    public GameObject[] trees;
    public float speed;
    public Vector3 position;
    bool isstop = false;

    public void stop()
    {
        isstop = true;
    }

    public void restart()
    {
        isstop = false;
    }

    public void rotate(int direction)
    {
        if (tree1 != null)
            tree1.transform.RotateAround(runner.position, new Vector3(0, 1, 0), 90 * direction);
        if (tree2 != null)
            tree2.transform.RotateAround(runner.position, new Vector3(0, 1, 0), 90 * direction);
        if (tree3 != null)
            tree3.transform.RotateAround(runner.position, new Vector3(0, 1, 0), 90 * direction);
    }

    // Use this for initialization
    void Start () {
        position = runner.position;

        Vector3 pos1 = new Vector3(position.x + 13, 0, position.z + 600);
        Vector3 pos2 = new Vector3(position.x - 12, 0, position.z + 480);
        Vector3 pos3 = new Vector3(position.x + 11, 0, position.z + 300);

        tree1 = Instantiate(trees[0], pos1, Quaternion.identity) as GameObject;
        tree2 = Instantiate(trees[0], pos2, Quaternion.identity) as GameObject;
        tree3 = Instantiate(trees[0], pos3, Quaternion.identity) as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        position = runner.position;
        if(!isstop)
            CheckTree();
    }


    void CheckTree()
    {
        if (tree1.transform.position.z <= position.z)
        {
            Destroy(tree1);
            Vector3 pos1 = new Vector3(position.x + 13, 0, position.z + 800);
            tree1 = Instantiate(trees[0], pos1, Quaternion.identity) as GameObject;
            tree1.transform.parent = transform;
        }
        if (tree2.transform.position.z <= position.z)
        {
            Destroy(tree2);
            Vector3 pos1 = new Vector3(position.x - 12, 0, position.z + 780);
            tree2 = Instantiate(trees[1], pos1, Quaternion.identity) as GameObject;
            tree2.transform.parent = transform;
        }
        if (tree3.transform.position.z <= position.z)
        {
            Destroy(tree3);
            Vector3 pos1 = new Vector3(position.x + 11, 0, position.z + 850);
            tree3 = Instantiate(trees[0], pos1, Quaternion.identity) as GameObject;
            tree3.transform.parent = transform;
        }
    }

}

﻿using UnityEngine;
using System.Collections;

public class RoadManager : MonoBehaviour {

    public GameManager game;
    public Runner runner;
    public GameObject Road1;
    public GameObject Road2;
    public GameObject Road3;
    public GameObject[] Roads;
    GameObject temp;
    public Vector3 position;
    public Vector3 RunDirection;

    public int counter = 0;
    public int isTroad = 0;
    bool rotateafter = false;
    bool isstop = false;

    // Use this for initialization
    void Start() {
        position = runner.position;
    }

    // Update is called once per frame
    void Update() {

        position = runner.position;
        RunDirection = new Vector3(0f, 0f, 1f);
        if (!isstop)
            MoveGround();
    }


    public void stop()
    {
        isstop = true;
    }

    public void restart()
    {
        isstop = false;
    }

    public void rotate(int direction)
    {
        if (Road2 != null)
        {
            Road2.transform.RotateAround(runner.position, new Vector3(0, 1, 0), 90 * direction);
            //Change the data of RunDirection vector according to the rotation
            if (RunDirection.z == 1f)
            {
                if(direction == 1)
                {
                    RunDirection = new Vector3(1f, 0f, 0f);
                }
                else if(direction == -1)
                {
                    RunDirection = new Vector3(-1f, 0f, 0f);
                }
            }
            else if (RunDirection.z == -1f)
            {
                if (direction == 1)
                {
                    RunDirection = new Vector3(-1f, 0f, 0f);
                }
                else if (direction == -1)
                {
                    RunDirection = new Vector3(1f, 0f, 0f);
                }
            }
            else if (RunDirection.x == 1f)
            {
                if (direction == 1)
                {
                    RunDirection = new Vector3(0f, 0f, -1f);
                }
                else if (direction == -1)
                {
                    RunDirection = new Vector3(0f, 0f, 1f);
                }
            }
            else if (RunDirection.x == -1f)
            {
                if (direction == 1)
                {
                    RunDirection = new Vector3(0f, 0f, 1f);
                }
                else if (direction == -1)
                {
                    RunDirection = new Vector3(0f, 0f, -1f);
                }
            }
            //----------------------------------------------------------
        }
        Destroy(Road1);

        temp = Road2;

        Vector3 pos = new Vector3(position.x, 1, 400 + position.z); //+ 400 * RunDirection;
        Road2 = Instantiate(Roads[0], pos, Quaternion.identity) as GameObject;
        Road2.transform.parent = transform;
        Vector3 pos2 = new Vector3(position.x, 1, 800 + position.z); //+ 800 * RunDirection;
        Road1 = Instantiate(Roads[0], pos2, Quaternion.identity) as GameObject;
        Road1.transform.parent = transform;

        counter = 0;
        isTroad = 0;
        rotateafter = true;
        runner.nojump = false;
    }


    void MoveGround()
    {
        if (Road2.transform.position.z - position.z <= -200 && counter < 4)
        {
            counter++;
            Vector3 pos = new Vector3(position.x, 1, 580 + position.z);
            Road3 = Instantiate(Roads[0], pos, Quaternion.identity) as GameObject;
            if (isTroad == 2)
                Road3.SetActive(false);

            Road3.transform.parent = transform;
            DestroyGround();
        }
        else if (Road2.transform.position.z - position.z <= -200 && counter == 4)
        {
            counter++;
            Vector3 pos = new Vector3(position.x, 1, 580 + position.z);
            Road3 = Instantiate(Roads[4], pos, Quaternion.identity) as GameObject;
            Road3.transform.parent = transform;    
            isTroad = 3;
            DestroyGround();
        }
        else if (Road2.transform.position.z - position.z <= -200 && counter == 5)
        {
            counter = 0;
            Destroy(temp);
        }
    }

    void DestroyGround()
    {
        Destroy(Road2);
        Road2 = Road1;
        Road1 = Road3;
        if (isTroad > 0)
            isTroad--;

        if (isTroad == 2)
            runner.nojump = true;
    }
}

﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public int speed = 30;
    public int stage;
    public Runner runner;
    public RoadManager road;
    public Terrain_Manager terrain;
    public TreeObject tree;
    public bool isrotate = false;
    public int select = 0;


    public void rotate(int direction)
    {
        road.stop();
        terrain.stop();
        runner.stop();

        road.rotate(direction);
        //terrain.rotate(direction);
        tree.rotate(direction);

 
        road.restart();
        terrain.restart();
        tree.restart();
        runner.restart();
    }


	// Use this for initialization
	void Start () {
        select = 1; // Default is left
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
